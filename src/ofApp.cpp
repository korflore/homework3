#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    cam.setup(1280, 720);
    frame.allocate(1280, 720,OF_IMAGE_COLOR);
    for(int i = 0 ; i< 13; i++){
        test[i][0]=(i*100);
        test[i][1]=(0);
        test[i][2]=((i*100)+100);
        test[i][3]=(720);
        test[i][4]=(i*100);
        test[i][5]=(0);
        if(i%3 == 0){
            test[i][6] = 255;
            test[i][7] = 0;
            test[i][8] = 0;
        }else if (i%3 ==1){
            test[i][6] = 0;
            test[i][7] = 255;
            test[i][8] = 0;
        }else{
            test[i][6] = 0;
            test[i][7] = 0;
            test[i][8] = 255;
            
        }
    }
    gui.setup();
    
    gui.add(rotation.setup("Rotate/Mirror", 0, 0, 180));
    gui.add(move.setup("Move left/Right", 0, -12, 12));
    gui.add(randAll.setup("Randomize Positions"));
    gui.add(randColor.setup("Randomize Colors"));
    gui.add(rect.setup("Restart"));
}

//--------------------------------------------------------------
void ofApp::update(){
    if(cam.isInitialized()){
        cam.update();
        if(cam.isFrameNew()){
            frame.setFromPixels(cam.getPixels());
        }
    }
    
    cam.update();
    for(int i = 0 ; i< 13; i++){
        int temp = i + move;
               test[i][0]=test[i][0];
               test[i][1]=(0);
               test[i][2]=test[i][2];
               test[i][3]=(720);
               test[i][4]=((i+move)*100);
               test[i][5]=(0);
        
        if(temp%3 == 0){
            test[i][6] = test[i][6];
            test[i][7] = test[i][7];
            test[i][8] = test[i][8];
        }else if (temp%3 ==1){
            test[i][6] = test[i][6];
            test[i][7] = test[i][7];
            test[i][8] = test[i][8];
        }else{
            test[i][6] = test[i][6];
            test[i][7] = test[i][7];
            test[i][8] = test[i][8];
            
        }
    }
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    
    frame.rotate90(rotation);
    
    if(randAll){
        for(int i = 0 ; i< 13; i++){
            int r = ofRandom(12);
            test[r][0]=(i*100);
            test[r][1]=(0);
            test[r][2]=((i*100)+100);
            test[r][3]=(720);
            test[r][4]=(r*100);
            test[r][5]=(0);
            
            
            
            ofSetColor(test[i][6], test[i][7],test[i][8]);
            frame.drawSubsection(test[i][0], test[i][1], test[i][2], test[i][3], test[i][4], test[i][5]);
        }
        
    }else if(randColor){
        for(int i = 0; i< 13; i++){
            
            if(i%3 == 0){
                test[i][6] = ofRandom(255);
                test[i][7] = ofRandom(255);
                test[i][8] = ofRandom(255);
            }else if (i%3 ==1){
                test[i][6] = ofRandom(255);
                test[i][7] = ofRandom(255);
                test[i][8] = ofRandom(255);
            }else{
                test[i][6] = ofRandom(255);
                test[i][7] = ofRandom(255);
                test[i][8] = ofRandom(255);
                
            }
        }
    }else if(rect){
        setup();
    } else{
        for(int i = 0; i < 13; i++){
            int r = ofRandom(12);
            ofSetColor(test[i][6], test[i][7],test[i][8]);
            frame.drawSubsection(test[i][0], test[i][1], test[i][2], test[i][3], test[i][4], test[i][5]);
        }
    }
    
    
    gui.draw();
    
}
