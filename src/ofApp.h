#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

    ofVideoGrabber cam;
    ofImage frame;
    
    ofxPanel gui;
    ofxButton randAll;
    ofxButton randColor;
    ofxButton rect;
    ofxIntSlider rotation;
    ofxIntSlider move;
    int i=0;
    int test[13][9];
		
};
